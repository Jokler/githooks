# Githooks
This is a small utility designed to listen for gitlab and github webhooks and execute scripts on the system when those hooks are received. It was originally designed to automate the process of re-building a static site in response to merges.

This project is in **early alpha** and was largely written as an exercise in Rust.

## How to use
Once installed, `githooks` can be invoked from the command line with the following options:

```
githooks 0.1.0


USAGE:
    githooks [OPTIONS]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -c <config>          A config .yaml file for default configurations (overridden by command line options)
    -e <events>          A YAML file mapping events on repos to scripts to run
    -H <hostname>        The hostname to listen on (default: 127.0.0.1)
    -p <port>            The port to listen on (default: 9191
    -t <tokens>          A YAML file mapping repositories to tokens
```

All options other than `-c` can be specified in a `config.yml` file, which, by default, `githooks` will look for in `/etc/githooks/config.yml`. In order for it to do anything meaningful, you need to provide both a `tokens.yml` file and an `events.yml` file (both of which are looked for by default in `/etc/githooks`).

### tokens.yml

The `tokens.yml` file contains a mapping of repository names to API tokens, e.g.:

```yaml
blog.mysite.com:
    - KyGALa8CZqsoePjJsmwQH6nO56-W8hrhO6INth0bDCMohpjLIt1L6ppnqGGa33zJBq2AfQTphWNvtByaiSKxyQ

mysite.com:
    - lHabDtLhks-tvhcyHtbcmhnvLSs4hRugc7K02M1fJFSvWHGCZS7iqldo25szKqa-wZxvsbpwSC4DrHlDh50foA
```

### events.yml

Gitlab (and github) webhooks can be triggered by various events (merge, pull, etc). This `events.yml` file provides the actions taken in response to every (repository, action) pair. References are supported in the YAML files so that multiple targets can do the same thing. Example:

```yaml

blog.mysite.com:
    push: &build
        cwd: '/var/www/blog.mysite.com'
        script:
            - 'git pull'
            - make html 
    merge: *build

mysite.com:
    push:
        cwd: '/var/www/mysite.com'
        script:
            - 'git pull'
            - make html
```

Currently the relevant variables are `cwd` (the directory in which to execute the script) and `script` (the script to execute). At the moment, no information other than the fact that a webhook event occurred is available for use in these scripts.

## Installation
The deployment strategy for this project is fairly weak and contributions on that front are *very welcome*. Currently, to install, clone the repository:

```bash
git clone git@gitlab.com:pganssle/githooks.git
```

Then enter the directory and build the project with [cargo](https://doc.rust-lang.org/cargo/):

```bash
cd githooks
cargo build --release
```

Finally, you can use the ``install.sh``:

**WARNING**: This is a convenience script I wrote - there is no corresponding "uninstall" script. Use at your own risk

```bash
# This will cp target/release/githooks into /bin and install a
# systemd service. It does *NOT* check if your system uses systemd
sudo ./install.sh
```

## License
This project is licensed under Apache Software License version 2.0.
