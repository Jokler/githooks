use std::collections::{HashSet, HashMap};

use std::str::FromStr;
use yaml_rust::Yaml;

use actions::Action;
use hook_events::{Repo, Token, Event, HookEvent};
use yaml_handler::{load_yaml_file, YamlLoadable};

type TokenSet = HashSet<Token>;
type EventMap = HashMap<Event, Action>;
type RepoMap = HashMap<Repo, RepoConfig>;

pub struct RepoConfig {
    tokens: TokenSet,
    events: EventMap,
}

pub struct HookEventConfig {
    repo_configs: HashMap<Repo, RepoConfig>,
}

impl RepoConfig {
    pub fn new(tokens: TokenSet, events: EventMap) -> RepoConfig {
        RepoConfig {
            tokens: tokens,
            events: events
        }
    }

    pub fn valid_token(&self, token: &Token) -> bool {
        self.tokens.contains(token)
    }
}

impl HookEventConfig {
    pub fn new(events_fpath: &str, tokens_fpath: &str) -> Result<HookEventConfig, String> {
        let mut map_out = RepoMap::new();

        let repo_events = load_repo_file(events_fpath);
        let repo_tokens = load_repo_file(tokens_fpath);

        for (repo, event_node) in repo_events.into_iter() {
            if !repo_tokens.contains_key(&repo) {
                return Err(format!("No tokens found for repo {}", repo));
            }
            let tokens_node = repo_tokens.get(&repo).unwrap();

            let tokens = TokenSet::from_yaml(&tokens_node);
            let events = EventMap::from_yaml(&event_node);

            map_out.insert(repo, RepoConfig::new(tokens, events));
        }

        let handler_out = HookEventConfig { repo_configs: map_out };

        Ok(handler_out)
    }

    pub fn execute_event(&self, hook_event: &HookEvent) {
        let repo = &hook_event.repo;
        let event = &hook_event.event;

        let config = match self.repo_configs.get(repo) {
            Some(c) => c,
            None => {
                println!("Unknown repository {}", repo);
                return;
            }
        };

        if !config.valid_token(&hook_event.token) {
            // Temporarily just print this - eventually log it
            println!("Invalid token for repo {}", repo); 
            return;
        }

        let act : &Action = match config.events.get(event) {
            Some(a) => a,
            None => {
                println!("No actions for event {} in repo {}",
                         event, repo);
                return;
            }
        };

        match act.execute() {
            Ok(output) => {
                println!("Action executed from repo {} for event {}",
                         repo, event);
                println!("  status: {}", output.status);
                println!("  stdout: {}", String::from_utf8_lossy(&output.stdout));
                println!("  stderr: {}", String::from_utf8_lossy(&output.stderr));
            },
            Err(e) => {
                println!("Error executing event {} for repo {}: {}",
                        event, repo, e);
            },
        };
    }    
}


impl YamlLoadable for TokenSet {
    fn from_yaml(node: &Yaml) -> TokenSet {
        let mut tokens_out : TokenSet  = TokenSet::new();

        let tokens = node.as_vec().unwrap();
        for token in tokens.into_iter() {
            let token_strip = token.as_str().unwrap().trim();

            tokens_out.insert(token_strip.to_string());
        }

        tokens_out
    }
}

impl YamlLoadable for EventMap {
    fn from_yaml(node: &Yaml) -> EventMap {
        let mut emap_out : EventMap = EventMap::new();
        let repo_map = node.as_hash().unwrap();

        for (event_node, action_node) in repo_map.into_iter() {
            let event = Event::from_str(event_node.as_str().unwrap()).unwrap();
            if emap_out.contains_key(&event) {
                panic!("Duplicate event key found: {}", event);
            }

            let action = Action::from_yaml(action_node);

            emap_out.insert(event, action);
        }

        emap_out
    }
}

impl YamlLoadable for Action {
    fn from_yaml(node: &Yaml) -> Action {
        let node_map = node.as_hash().unwrap();
        let cwd : String = node_map.get(&Yaml::from_str("cwd")).unwrap()
                            .as_str().unwrap().to_string();

        // The script needs to be a vector of strings:
        let script_node = node_map.get(&Yaml::from_str("script")).unwrap()
                            .as_vec().unwrap();

        let mut script : Vec<String> = Vec::new();
        for script_line_node in script_node.into_iter() {
            script.push(script_line_node.as_str().unwrap().to_string());
        }

        Action::new(cwd, script)
    }
}

fn load_repo_file(fpath: &str) -> HashMap<Repo, Yaml> {
    // TODO: Make this a coroutine so we don't have to copy everything into
    // a separate HashMap
    let yaml = load_yaml_file(fpath).unwrap();
    let root_node = &yaml[0].as_hash().unwrap();

    let mut map_out : HashMap<Repo, Yaml> = HashMap::new();
    for (repo_node, sub_node) in root_node.into_iter() {
        let repo = Repo::from_str(repo_node.as_str().unwrap()).unwrap();

        if map_out.contains_key(&repo) {
            panic!("Duplicate repo entry in config: {}", repo);
        }

        map_out.insert(repo, sub_node.clone());
    }

    map_out
}